<?php namespace Kurmangazy\SchoolGraduates\Models;

use Model;

/**
 * Model
 */
class Graduates extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kurmangazy_schoolgraduates_table';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $guarded = ['*'];

    public $translatable = ['fullname', 'state', 'city', 'university'];

    public static function boot()
    {
        // Call default functionality (required)
        parent::boot();

        // Check the translate plugin is installed
        if (!class_exists('RainLab\Translate\Behaviors\TranslatableModel'))
            return;

        // Extend the constructor of the model
        self::extend(function($model){
            // Implement the translatable behavior
            $model->implement[] = 'RainLab.Translate.Behaviors.TranslatableModel';
        });
    }
}
