<?php 

namespace Kurmangazy\SchoolGraduates\Components;

use Cms\Classes\ComponentBase;
use Kurmangazy\SchoolGraduates\Models\Graduates;

class SchoolGraduates extends ComponentBase
{
    public $grads;

    public function componentDetails()
    {
        return [
            'name'        => 'School graduates Overview',
            'description' => 'Shows an overview of school graduates'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $arr = [];
        $grads = Graduates::all();

        foreach ($grads as $key => $value) {
            if (array_key_exists($value['coordinates'], $arr)) {
                $arr[ $value['coordinates'] ]['students'][] = $value['fullname'].' (Class: '.$value['class'].')';
            }else{
                $arr[ $value['coordinates'] ] = [
                    'students' => [],
                    'state' => $value['state'],
                    'coordinates' => $value['coordinates'],
                    'city' => $value['city'],
                    'university' => $value['university'],
                    'city' => $value['city'],
                    'logo' => $value['logo'],
                ];

                $arr[ $value['coordinates'] ]['students'][] = $value['fullname'].' (Class: '.$value['class'].')';
            }
            
        }

        $this->grads = $arr;
    }
}
