<?php 

namespace Kurmangazy\SchoolGraduates;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'School Graduates',
            'description' => 'Adds the ability to add and show your school grads.',
            'author'      => 'Kurmangazy Kongratbayev',
            'icon'        => 'icon-building'
        ];
    }

    public function registerComponents()
    {
    	return [
            'Kurmangazy\SchoolGraduates\Components\SchoolGraduates' => 'SchoolGraduates',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation()
    {
        return [
            'partners' => [
                'label'       => 'School Graduates',
                'url'         => Backend::url('kurmangazy/schoolgraduates/schoolgraduates'),
                'icon'        => 'icon-university',
                'permissions' => ['kurmangazy.schoolgraduates.*'],
                'order'       => 500,
            ],
        ];
    }
}
