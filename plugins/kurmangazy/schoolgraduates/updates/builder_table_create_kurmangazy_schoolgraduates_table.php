<?php namespace Kurmangazy\SchoolGraduates\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKurmangazySchoolgraduatesTable extends Migration
{
    public function up()
    {
        Schema::create('kurmangazy_schoolgraduates_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('fullname');
            $table->string('class');
            $table->string('state');
            $table->string('city');
            $table->string('university');
            $table->string('coordinates');
            $table->string('logo');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kurmangazy_schoolgraduates_table');
    }
}
