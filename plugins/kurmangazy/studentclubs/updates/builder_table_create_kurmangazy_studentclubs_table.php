<?php namespace Kurmangazy\StudentClubs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKurmangazyStudentclubsTable extends Migration
{
    public function up()
    {
        Schema::create('kurmangazy_studentclubs_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 255);
            $table->text('description');
            $table->string('image', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kurmangazy_studentclubs_table');
    }
}
