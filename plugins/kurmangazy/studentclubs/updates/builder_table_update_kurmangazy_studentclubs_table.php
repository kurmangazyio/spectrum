<?php namespace Kurmangazy\StudentClubs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKurmangazyStudentclubsTable extends Migration
{
    public function up()
    {
        Schema::table('kurmangazy_studentclubs_table', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kurmangazy_studentclubs_table', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
