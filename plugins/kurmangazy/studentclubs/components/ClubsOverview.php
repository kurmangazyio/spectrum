<?php 

namespace Kurmangazy\StudentClubs\Components;

use Cms\Classes\ComponentBase;
use Kurmangazy\StudentClubs\Models\Clubs;

class ClubsOverview extends ComponentBase
{

    public $clubs;

    public function componentDetails()
    {
        return [
            'name'        => 'School clubs Overview',
            'description' => 'Shows an overview of school clubs'
        ];
    }

    public function defineProperties()
    {
        $options = array();
        for($i = 0; $i < 11 ;$i++){
            $options[$i] = $i;
        }
        $orderByOptions = array("title" => "Title", "created_at" => "Created At");
        $orderOptions = array("asc" => "Ascending", "desc" => "Descending");
        return [
            'maxItems' => [
                'title'       => 'Amount of clubs',
                'type'        => 'dropdown',
                'default'     => '0',
                'placeholder' => 'Select amount of clubs (0 = all clubs)',
                'options'     => $options
            ],
            'orderBy' => [
                'title'       => 'Order By',
                'type'        => 'dropdown',
                'default'     => 'created_at',
                'placeholder' => 'Order clubs by',
                'options'     => $orderByOptions
            ],
            'order' => [
                'title'       => 'Order',
                'type'        => 'dropdown',
                'default'     => 'desc',
                'placeholder' => 'Order clubs',
                'options'     => $orderOptions
            ]
        ];
    }

    public function onRun(){
        $maxItems = $this->property('maxItems');
        $orderBy = $this->property('orderBy');
        $order = $this->property('order');
        
        if(!$orderBy) { $orderBy = 'created_at'; }
        
        if(!$order) { $order = 'desc'; }
        
        /*if($maxItems != 0){
            $this->clubs = Clubs::orderBy($orderBy, $order)->take($maxItems)->get();
        }else{
            $this->clubs = Clubs::all();
        }*/
        $this->clubs = Clubs::orderBy($orderBy, $order)->get();
    }
}