<?php namespace Kurmangazy\StudentClubs;

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'School Clubs',
            'description' => 'Adds the ability to add and show your school clubs.',
            'author'      => 'Kurmangazy Kongratbayev',
            'icon'        => 'icon-building'
        ];
    }

    public function registerComponents()
    {
    	return [
            'Kurmangazy\StudentClubs\Components\ClubsOverview' => 'ClubsOverview',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation()
    {
        return [
            'partners' => [
                'label'       => 'School Clubs',
                'url'         => Backend::url('kurmangazy/studentclubs/studentclubs'),
                'icon'        => 'icon-building',
                'permissions' => ['kurmangazy.studentclubs.*'],
                'order'       => 500,
            ],
        ];
    }
}
