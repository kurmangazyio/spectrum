<?php namespace Kurmangazy\SpectrumEmployees\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKurmangazySpectrumemployeesTable extends Migration
{
    public function up()
    {
        Schema::create('kurmangazy_spectrumemployees_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname', 255);
            $table->string('email', 255);
            $table->string('image', 255);
            $table->string('positiondivider', 255);
            $table->string('position', 255);
            $table->string('education', 55);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kurmangazy_spectrumemployees_table');
    }
}
