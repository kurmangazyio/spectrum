<?php 

namespace Kurmangazy\SpectrumEmployees\Components;

use Cms\Classes\ComponentBase;
use Kurmangazy\SpectrumEmployees\Models\Employees;

class SpectrumEmployees extends ComponentBase
{
    public $employees;
    public $divider = [
    	'Senior Leadership Team',
		'Teachers',
        'Homeroom Teachers',
        'Teaching Assistants'
    ];

    public function componentDetails()
    {
        return [
            'name'        => 'School employees Overview',
            'description' => 'Shows an overview of school employees'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->employees = Employees::all();
    }
}
