<?php namespace Kurmangazy\SpectrumEmployees;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'School Employees',
            'description' => 'Adds the ability to add and show your school emplyees.',
            'author'      => 'Kurmangazy Kongratbayev',
            'icon'        => 'icon-building'
        ];
    }

    public function registerComponents()
    {
    	return [
            'Kurmangazy\SpectrumEmployees\Components\SpectrumEmployees' => 'SpectrumEmployees',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation()
    {
        return [
            'partners' => [
                'label'       => 'School Employees',
                'url'         => Backend::url('kurmangazy/spectrumemployees/spectrumemployees'),
                'icon'        => 'icon-group',
                'permissions' => ['kurmangazy.spectrumemployees.*'],
                'order'       => 500,
            ],
        ];
    }
}
