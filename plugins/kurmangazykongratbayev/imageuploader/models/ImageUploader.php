<?php namespace KurmangazyKongratbayev\Imageuploader\Models;

use Model;

/**
 * Model
 */
class ImageUploader extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kurmangazykongratbayev_imageuploader_table';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
