<?php namespace KurmangazyKongratbayev\Imageuploader\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKurmangazykongratbayevImageuploaderTable extends Migration
{
    public function up()
    {
        Schema::create('kurmangazykongratbayev_imageuploader_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('imageid');
            $table->text('path');
            $table->string('created_at');
            $table->string('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kurmangazykongratbayev_imageuploader_table');
    }
}
