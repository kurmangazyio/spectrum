<?php 

namespace KurmangazyKongratbayev\Imageuploader\Components;

use Cms\Classes\ComponentBase;
use KurmangazyKongratbayev\Imageuploader\Models\ImageUploader;

class ImageuploaderComponent extends ComponentBase
{
    public $imageid;

    public function componentDetails()
    {
        return [
            'name'        => 'Image Setter',
            'description' => 'Setup image from media'
        ];
    }

    public function defineProperties()
    {
        return [
            'imageid' => [
                'title'       => 'Image ID',
                'description' => 'Type here image id to display',
                'type'        => 'string',
                'default'     => ''
            ]
        ];
    }

    public function onRun(){
        $id = $this->property('imageid');
        
        if($id == '') { 
            $this->imageid = ImageUploader::where('imageid', $id)->first();
        }else {
            $this->imageid = ImageUploader::first();
        }
    }
}
