<?php namespace KurmangazyKongratbayev\Imageuploader;

use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Image Setter',
            'description' => 'Adds the ability to add and show your images.',
            'author'      => 'Kurmangazy Kongratbayev',
            'icon'        => 'icon-copy'
        ];
    }

    public function registerComponents()
    {
        return [
            'KurmangazyKongratbayev\Imageuploader\Components\ImageuploaderComponent' => 'ImageuploaderComponent',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation()
    {
        return [
            'partners' => [
                'label'       => 'Image Setter',
                'url'         => Backend::url('kurmangazykongratbayev/imageuploader/imageuploader'),
                'icon'        => 'icon-copy',
                'permissions' => ['kurmangazykongratbayev.imageuploader.*'],
                'order'       => 500,
            ],
        ];
    }
}
